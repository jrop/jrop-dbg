import { inspect } from "node:util";

export const dbg = <T>(x: T, msg?: string): T => {
  const caller = new Error().stack?.split("\n")[2].trim();
  console.error(
    `dbg!!!${msg ? ` ${msg}` : ""}`,
    inspect(x, { depth: 3, colors: true }),
    caller ? `(${caller})` : "",
  );
  return x;
};
export default dbg;
